// ==UserScript==
// @copyright           ©2014 - 2017, Damian Ho
// @description         Helper script for Bazaar.tf
// @downloadURL         https://bitbucket.org/damianhxy/bazaar.tf-enhancer/raw/master/auction.user.js
// @grant               GM_xmlhttpRequest
// @include             /^https:\/\/bazaar\.tf.*$/
// @name                Bazaar.tf Enhancer
// @namespace           https://bitbucket.org/damianhxy
// @noframes
// @run-at              document-end
// @version             1.5.2.7
// ==/UserScript==

(function () {
    /*********** SETTINGS ***********/
    var updateFreq = 3;              //[days] price refresh frequency
    /******** END OF SETTINGS *******/

    function annotate(trade) {
        Array.prototype.forEach.call(trade, e => {
            if (+e.getAttribute("data-defindex") < 0 || e.getAttribute("data-game") !== "440")
                return;
            var price = getPrice(e),
                text = "",
                name = e.getAttribute("data-name"),
                details = e.getAttribute("data-details") || "",
                extra = e.querySelector(".item-number"),
                specialText = e.getAttribute("data-special") === "Low Craft #" || details.includes("Killstreak");
            if (extra && !extra.textContent.includes("x"))
                extra.remove();
            if (e.querySelector(".ks"))
                e.querySelector(".ks").remove();
            price[0] = trunc(round(+price[0]));
            if (e.hasAttribute("data-crate"))
                text = `#${e.getAttribute("data-crate")}: `;
            if (specialText) {
                if (e.getAttribute("data-special") === "Low Craft #")
                    text = `Lowcraft #${e.getAttribute("data-craft")}`;
                if (details.includes("Killstreak")) {
                    var colour = "darkgoldenrod";
                    if (details.includes("Specialized"))
                        colour = "darkgray";
                    else if (details.includes("Professional"))
                        colour = "gold";
                    e.appendChild(createIcon("bomb", `color: ${colour}; background-color: white;`));
                    text = "Killstreak";
                }
            } else if (name.includes("Metal"))
                text = `${price[0]} Refined`;
            else if (price[0]) {
                var currency = e.getAttribute("data-price").split(" ")[1];
                if (currency.includes("Weapon"))
                    text += "Any Weapon";
                else if (currency.includes("Key"))
                    text += `${price[1].toFixed(2)} ${currency}`;
                else
                    text += `${price[0]} ${currency}`;
            } else
                text += "Unpriced";
            if (e.hasAttribute("data-notes")) {
                var notes = e.getAttribute("data-notes"),
                    count = notes.match(/<\\\/p>/g).length;
                if (notes.includes("Gifted"))
                    e.appendChild(createIcon("gift"));
                if (notes.includes("Painted:") && !notes.includes(name))
                    e.appendChild(createIcon("paint-brush"));
                count -= (notes.match(/Tradable|(?:Craft|Gift|Paint|Festiviz)ed/g) || "").length;
                if (e.classList.contains("q11-440") && count > 1)
                    e.appendChild(createIcon("cogs"));
            }
            if (e.hasAttribute("data-spells")) {
                e.querySelector(".spell").remove();
                e.appendChild(createIcon("moon-o"));
            }
            var txt = document.createElement("span");
            txt.classList.add("item-text");
            txt.style = "bottom: 0; left: 0; right: 0;";
            txt.appendChild(document.createTextNode(text));
            e.appendChild(txt);
        });
    }

    function calc(trade) {
        var total = 0,
            allPure = true;
        Array.prototype.forEach.call(trade, e => {
            var response = getPrice(e);
            total += response[0];
            allPure &= response[3];
        });
        total = trunc(round(total));
        var string = `${total && !allPure ? "~" : ""} ${total} Refined`;
        if (total > +keyP.toFixed(2))
            string += `/ ${(total / keyP).toFixed(2)} Keys`;
        return string;
    }

    function check(predicate, callback) {
        predicate() ? callback() : setTimeout(() => check(predicate, callback), 500);
    }

    function createIcon(cls, style) {
        var icon = document.createElement("i");
        icon.classList.add("fa", `fa-${cls}`, "fa-inverse", "fa-border", "pull-left");
        icon.style = `border-radius: 50%;${style ? " " + style : ""}`;
        return icon;
    }

    function getAPIPrice(name) {
        var data = JSON.parse(localStorage.prices);
        var item = data.response.items[name];
        if (!item)
            item = data.response.items[`Strange Part: ${name}`];
        if (!item)
            item = data.response.items[`Strange Cosmetic Part: ${name}`];
        if (!item)
            return 0;
        var obj = item.prices[6].Tradable.Craftable[0];

        var currency = obj.currency;
        var value = obj.value;
        if (currency === "keys")
            value *= keyP;
        return value;
    }

    function getPrice(ele) {
        var price = 0,
            original = 0,
            hasRange = false,
            isPure = false,
            name = ele.getAttribute("data-name");
        if (ele.hasAttribute("data-price")) {
            var data = ele.getAttribute("data-price").split(" ");
            if (data[0] === "Any")
                price = 1 / 18;
            else if (data[0].includes("-")) {
                var pts = data[0].split("-");
                price = original = (+pts[0] + +pts[1]) / 2;
                hasRange = true;
            } else
                price = original = +data[0];
            if (name.match(/(?:Key|(?:Slot|Class) Token)/))
                isPure = true;
            if (data[1].includes("Key"))
                price *= keyP;
            else if (name.match(/(?:Slot|Class) Token/))
                price = 1 / 6;
            if (!isPure && !hasRange)
                price = round(price);
        } else {
            var index = ["Refined", "Reclaimed", "Scrap"].findIndex(e => `${e} Metal` === name);
            if (~ index) {
                isPure = true;
                price = 1 / Math.pow(3, index);
            }
        }
        return [price, original, hasRange, isPure];
    }

    function round(num) {
        if (num <= 5 / 90)
            return num;
        num += num * 10 % 1 / 90;
        return Math.floor(num) + Math.round((num % 1 * 9).toFixed(2)) / 9;
    }

    function trunc(num) {
        return Math.floor(num * 100) / 100;
    }

    function createSummary(txt, parent) {
        var paints = {},
            parts = {};
        txt.style.cursor = "pointer";
        Array.prototype.forEach.call(parent.querySelectorAll("[data-paint]"), e => {
            var name = e.getAttribute("data-notes").split("<\\\/p>")[0].slice(13);
            if (name !== e.getAttribute("data-name"))
                paints[name] = (paints[name] || 0) + 1;
        });
        Array.prototype.forEach.call(parent.querySelectorAll(".fa-cogs"), e => {
            var itemParts = e.parentNode.getAttribute("data-notes").match(/<p>.+?:/g).slice(1);
            itemParts = itemParts.map(f => f.substr(3, f.length - 4));
            itemParts.forEach(f => {
                if (f !== "Points Scored") // In case strange cosmetic is painted
                    parts[f] = (parts[f] || 0) + 1;
            });
        });
        txt.addEventListener("click", () => {
            var msg = "";
            var paintValue = 0;
            var partValue = 0;
            msg += `Painted: ${Object.keys(paints).length}\n`;
            for (var paint in paints) {
                msg += `[${("0" + paints[paint]).slice(-2)}] ${paint}\n`;
                paintValue += getAPIPrice(paint) * paints[paint];
            }
            msg += `Total paint value: ${trunc(round(paintValue / 2))} Refined\n`;
            msg += `Killstreaked: ${parent.getElementsByClassName("fa-bomb").length}\n`;
            msg += `Parted: ${parent.getElementsByClassName("fa-cogs").length} [Parts: ${Object.keys(parts).length}]\n`;
            for (var part in parts) {
                msg += `[${("0" + parts[part]).slice(-2)}] ${part}\n`;
                partValue += getAPIPrice(part) * parts[part];
            }
            msg += `Total part value: ${trunc(round(partValue / 2))} Refined\n`;
            msg += `Gifted: ${parent.querySelectorAll(".fa-gift").length}\n`;
            msg += `Spelled: ${parent.querySelectorAll("[data-spells]").length}\n`;
            msg += `Uncraftable: ${parent.querySelectorAll("[data-flag_cannot_craft]").length}\n`;
            msg += `Untradable: ${parent.querySelectorAll("[data-flag_cannot_trade]").length}\n`;
            msg += `Item Count: ${parent.childElementCount}\n`;
            alert(msg);
        });
    }

    function update() {
        if (document.getElementById("UserLoginForm"))
            return console.info(`Bazaar.tf Script Paused (Signed Out). Version ${GM_info.script.version}`);
        if (!localStorage.getItem("apikey"))
            localStorage.setItem("apikey", prompt("API key not set, register yours from http://backpack.tf/developer/"));
        if (localStorage.getItem("apikey").length !== 24) {
            alert("Incorrect API key length");
            localStorage.removeItem("apikey");
            update();
        }
        GM_xmlhttpRequest({
            method: "GET",
            url: `http://backpack.tf/api/IGetPrices/v4/?key=${localStorage.getItem("apikey")}`,
            onload: e => {
                var data = JSON.parse(e.response);
                if (!data.response.success)
                    return console.info(`Error:\n${data.response.message}`);
                localStorage.setItem("prices", e.response);
                var key = data.response.items["Mann Co. Supply Crate Key"].prices[6].Tradable.Craftable[0],
                    k = (key.value + (key.value_high || key.value)) / 2,
                    kDiff = ` (${k === keyP ? "=" : k < keyP ? "↓" : "↑"})`;
                localStorage.setItem("keyP", k);
                console.info(`Prices updated, Keys: ${k} Refined ${kDiff}`);
                localStorage.setItem("lastupdt", Date.now());
            }
        });
    }

    try {
        /******** SCRIPT GLOBALS ********/
        var tradeView = document.querySelector(".fa-reply"),
            tradeList = document.querySelector(".fa-share"),
            backpack = /^http:\/\/bazaar\.tf\/backpack\/\d{17}$/.test(document.URL),
            keyP = +localStorage.getItem("keyP"),
            lastUpdt = Date.now() - (+localStorage.getItem("lastupdt") || 0);
        /****** END SCRIPT GLOBALS ******/

        if (keyP && lastUpdt < 864e5 * updateFreq || update()) {
            var diff = 864e5 * updateFreq - lastUpdt,
                days = Math.floor(diff / 864e5),
                hours = Math.floor((diff % 864e5) / 36e5),
                minutes = Math.floor((diff % 36e5) / 6e4);
            console.info(`Prices will update in ${days} day(s), ${hours} hour(s) and ${minutes} min(s)`);
        }

        if (backpack)
            check(() => !document.querySelector(".fa-spinner"),
                  () => annotate(document.querySelectorAll(".item:not(.placeholder-icon)")));
        else if (tradeList || tradeView)
            annotate(document.getElementsByClassName("item"));
        if (tradeList) {
            Array.prototype.forEach.call(document.querySelectorAll("[data-type='more-items-icon']"), e => {
                var parent = e.parentNode;
                e.addEventListener("click", () => {
                    check(() => !document.querySelector(".progress"),
                          () => annotate(parent.children));
                });
            });
        } else if (tradeView) {
            var data = document.createElement("li"),
                offered = document.querySelector(".offered-items");
            data.style.color = "orange";
            data.appendChild(document.createTextNode(calc(offered.children)));
            createSummary(data, offered);
            document.querySelector(".trade-stats").appendChild(data);
            Array.prototype.forEach.call(document.querySelectorAll("[id^='thread-']"), e => {
                if (e.querySelector(".comment-offers")) {
                    var data = document.createElement("span"),
                        offered = e.querySelector(".item-list"),
                        notes = e.querySelector(".comment-content:not(.top-small-margin)");
                    data.style = "color: orange; float: right;";
                    data.appendChild(document.createTextNode(calc(offered.children)));
                    createSummary(data, offered);
                    notes.insertBefore(data, notes.firstChild);
                }
                if (e.querySelector(".btn-success.disabled"))
                    e.firstElementChild.style.borderColor = "lime";
                else if (e.querySelector(".btn-warning.disabled"))
                    e.firstElementChild.style.borderColor = "orange";
            });
        }
        console.info(`Bazaar.tf Script Loaded. Version ${GM_info.script.version}`);
    } catch(e) {
        alert(`[A fatal error has occured]\n${e.message}\n[Stack Trace]\n${e.stack}`);
    }
})();
