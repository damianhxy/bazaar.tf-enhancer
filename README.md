#Bazaar.tf-Enhancer
====================

Evaluates the value of auction items and offers

This script automatically sums up the value of items on [bazaar.tf](http://bazaar.tf)'s auctions

More information is available in the wiki.

Track its development here: [Trello](https://trello.com/b/txEgdznu/bazaar-tf-enhancer)